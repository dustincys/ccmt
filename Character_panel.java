﻿package p1;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Character_panel extends JFrame implements  ActionListener {
		public Character_panel(){
				super("多功能字符字工具_________yanshuoc@Gmail.com");
				initial();
		}
		private static final long serialVersionUID = 1L;
		private JPanel				jp;
		private JPanel				toolpanel;
		private TextAreaMenu			jt_in,jt_out;
		private JButton				jb1;
		private JButton				jb2;
		private JButton				jb3;
		private JButton				jb4;
		
		
		private JScrollPane			sc1,sc2;
		private JComboBox			jcb_font;
		private JComboBox			jcb_erb;
		private JComboBox			jcb_dotarraysize;//点阵型号12、14、16
		private JComboBox			jcb_enable_special_character;
		private JComboBox			jcb_dc;
		private JComboBox			jcb_setTF1,jcb_setTF2;
		
		private String[]				str_QQ_emotion_chinese=
		{
				"■","□","微笑","色","流泪","发怒","龇牙",
				"难过","抓狂","吐","可爱","流汗",
				"疑问","衰","敲打","鼓掌","坏笑",
				"西瓜","红心","地雷","大便","夜晚"				
		};
		private String[]				str_QQ_emotion_character=
		{
				"■","□","/wx","/se","/ll","/fn","/cy",
				"/ng","/zk","/tuu","/ka","/lh",
				"/yiw","/shuai","/qiao","/gz","/huaix",
				"/xig","/xin","/zhd","/bb","/yl"
		};
		
		private String[]				str_single_double_character={"单符","双符","三符","四符"};
		private String[]				str_enable_special_character={"本身符","填充符"};			
		private String[]				str_dotarraysize={"hzk12","hzk14","hzk16"};			
		private String[]				str_fontname={"小字体","中字体","大字体"};	
		private String[]				str_erbname={"实体字","镂空字"};			
		private Font[]					fonts={new Font("小字体",Font.PLAIN,12),new Font("中字体",Font.PLAIN,20),new Font("大字体",Font.PLAIN,30)}; 
		
		private boolean				erb_flag=true;//实体字镂空字的状态字
		private boolean				enable_special_character_flag=false;//实体字镂空字的状态字		
		private int					num_dotarraysize=0;//点阵选择
		private JTextField				jtf_sc1,jtf_sc2;
		private String 				str_special_character1="■",str_special_character2="□";
		private int					dc_flag=1;//双字符标志
		
		private ImageIcon				icon=new ImageIcon("1.png"); 
		private COP					cop=new COP();
		
		private void initial(){
				this.setBounds(100, 100, 600, 500);
				this.setIconImage(icon.getImage());
						jp=new JPanel();
						jp.setBounds(0, 0, 600, 500);
						jp.setLayout(null);
								jt_in=new TextAreaMenu();
								sc1=new JScrollPane(jt_in);	
								sc1.setBounds(10, 350, 575, 100);								
								toolpanel=new JPanel();
								toolpanel.setBounds(118, 310, 350, 40);
								toolpanel.setLayout(new GridLayout(2,4));
									jb1=new JButton("to字符字");
									jb1.addActionListener(this);
//									jb1.setBackground(new Color(250,250,250));
									jb1.setBounds(18, 310, 100, 40);
									jb2=new JButton("to镜像");
									jb2.addActionListener(this);
									jb2.setBounds(468, 310, 100, 20);
									jb4=new JButton("to图片字");
									jb4.addActionListener(this);
									jb4.setBounds(468, 330, 100, 20);
									
									
									jcb_dotarraysize=new JComboBox(this.str_dotarraysize);
									jcb_dotarraysize.addActionListener(this);
									jcb_font=new JComboBox(this.str_fontname);
									jcb_font.addActionListener(this);	
									jcb_erb=new JComboBox(this.str_erbname);
									jcb_erb.addActionListener(this);
									
									jcb_setTF1=new JComboBox(this.str_QQ_emotion_chinese);									
//									jcb_setTF1.setVisible(false);
									jcb_setTF1.addActionListener(this);
									jcb_setTF2=new JComboBox(this.str_QQ_emotion_chinese);
									jcb_setTF2.setSelectedIndex(1);
//									jcb_setTF2.setVisible(false);
									jcb_setTF2.addActionListener(this);
									
									jcb_dc=new JComboBox(this.str_single_double_character);
									jcb_dc.addActionListener(this);
									
									jcb_enable_special_character=new JComboBox(this.str_enable_special_character);
									jcb_enable_special_character.addActionListener(this);
									
									jtf_sc1=new JTextField(str_special_character1);
//									jtf_sc1.setVisible(false);
									jtf_sc2=new JTextField(str_special_character2);
//									jtf_sc2.setVisible(false);
									jb3=new JButton("置空");
									jb3.addActionListener(this);
									
//								toolpanel.add(jb1);
								toolpanel.add(jcb_dotarraysize);
								toolpanel.add(jcb_erb);
							
								toolpanel.add(jcb_enable_special_character);
								toolpanel.add(jtf_sc1);
								toolpanel.add(jtf_sc2);								
								
//								toolpanel.add(jb2);
								toolpanel.add(jcb_font);								
								toolpanel.add(jcb_dc);
								
								toolpanel.add(jb3);
								toolpanel.add(jcb_setTF1);
								toolpanel.add(jcb_setTF2);
																
								jt_out=new TextAreaMenu(){
									private static final long serialVersionUID = -2308615404205560110L;  
									public void mouseClicked(MouseEvent e) {  
										if(e.getButton()==MouseEvent.BUTTON1)
											this.selectAll();
									} 
								};
								jt_out.setEditable(false);
								sc2=new JScrollPane(jt_out);	
								sc2.setBounds(10, 10, 575, 300);
								

						jp.add(sc2);
						jp.add(jb1);
						jp.add(toolpanel);
						jp.add(jb2);
						jp.add(jb4);						
						jp.add(sc1);	
				this.add(jp);		
				this.setVisible(true);
				this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				this.setResizable(false); 
		}
		public void actionPerformed(ActionEvent e){
				if(e.getSource() instanceof JButton ){
						String str_in=jt_in.getText();
						if(str_in==null||str_in.equals("")){
								jt_out.setText("WoOoOoOo.....!\nMirror said null!!~~");
								return;
						}				
						if(e.getSource()==jb1){
							str_special_character1=jtf_sc1.getText();
							str_special_character2=jtf_sc2.getText();							
							jt_out.setText(toCharacter(str_in));	
						}else if(e.getSource()==jb2){
							jt_out.setText(tomirror(str_in));	
						}else if(e.getSource()==jb3){
							jt_in.setText("");
							jt_out.setText("");						
						}else if(e.getSource()==jb4){
							jt_out.setText(toCharPic(str_in));					
						}
				}else if(e.getSource() instanceof JComboBox){
						if(e.getSource()==jcb_erb){
							if(jcb_erb.getSelectedItem().toString().equals("实体字"))
								this.erb_flag=true;
							else this.erb_flag=false;
						}else if(e.getSource()==jcb_font){
							for(int i=0;i<fonts.length;i++){
									if(jcb_font.getSelectedItem().toString().equals(fonts[i].getName())){
											jt_in.setFont(fonts[i]);
											break;
									}
							}
						}else if(e.getSource()==jcb_dotarraysize){
							for(int i=0;i<str_dotarraysize.length;i++){
								if(jcb_dotarraysize.getSelectedItem().toString().equals(str_dotarraysize[i])){
									num_dotarraysize=i;
								}
							}
						}else if(e.getSource()==jcb_enable_special_character){
							if(jcb_enable_special_character.getSelectedItem().toString().equals(str_enable_special_character[0])){
								enable_special_character_flag=false;
//								this.jtf_sc1.setVisible(false);
//								this.jtf_sc2.setVisible(false);
//								this.jcb_setTF1.setVisible(false);
//								this.jcb_setTF2.setVisible(false);
								
							}else{
								enable_special_character_flag=true;
//								this.jtf_sc1.setVisible(true);
//								this.jtf_sc2.setVisible(true);
//								this.jcb_setTF1.setVisible(true);
//								this.jcb_setTF2.setVisible(true);								
							}							
						}else if(e.getSource()==this.jcb_dc){
							for(int i=0;i<this.str_single_double_character.length;i++){
								if(jcb_dc.getSelectedItem().toString().equals(str_single_double_character[i])){
									this.dc_flag=i+1;
								}
							}
						}else if(e.getSource()==this.jcb_setTF1){
							for(int i=0;i<this.str_QQ_emotion_chinese.length;i++){
								if(this.jcb_setTF1.getSelectedItem().toString().equals(str_QQ_emotion_chinese[i])){
									this.jtf_sc1.setText(str_QQ_emotion_character[i]);
								}
							}							
						}else if(e.getSource()==this.jcb_setTF2){
							for(int i=0;i<this.str_QQ_emotion_chinese.length;i++){
								if(this.jcb_setTF2.getSelectedItem().toString().equals(str_QQ_emotion_chinese[i])){
									this.jtf_sc2.setText(str_QQ_emotion_character[i]);
								}
							}
						}
				}
		}
		private String toCharacter(String str_in){
			/*
			 * 每次读取一行
			 */	
				if(str_in==null||str_in.equals("")){
						return "";
				}
				else{//过滤掉非法字符
					String str_out="";
					try{
						boolean  onceagain=true;						
						String temp1=str_in, temp2=str_in;
						
						while(onceagain){							
								if(temp1.contains("\n")){
										temp2=temp1.substring(0, temp1.indexOf("\n"));
										temp1=temp1.substring(temp1.indexOf("\n")+1, temp1.length());
								}
								else{
										temp2=temp1;
										onceagain=false;
								}
								str_out+=merge_Dot_Map(temp2);
								//需要待转换一行字符、镂空标志、点阵大小标志
								if(onceagain) str_out+="\n\n";
						}						
						return str_out;	
					}catch(Exception e){
						str_out="含有非汉字字符！";
					}					
					return str_out;
				}		
		}
		private String merge_Dot_Map(String oneline_in) throws Exception{
			if(oneline_in==null||oneline_in.equals("")){
				return "";
			}			
			String str_Map="";
			byte[]  s;
			int n_array=(num_dotarraysize<3?DotMap.arraysize[num_dotarraysize]/2:24);
			for(int i=0;i<n_array;i++){//打印的点阵行数
				for(int j=0;j<oneline_in.length();j++){
					s=DotMap.getDotMap(oneline_in.charAt(j),num_dotarraysize);
					str_Map+=getBinaryStrFromByte(s,i,oneline_in.charAt(j));
					if(j<oneline_in.length()-1)
						str_Map+="　";
				}
				if(i+1<n_array) str_Map+="\n";
			}
			return str_Map;	
		}
		private String getBinaryStrFromByte(byte[] s,int i,char c){//返回阵列S中的第i行字符
			String str_return="";			
			String s1=!enable_special_character_flag?(erb_flag?String.valueOf(c):"　"):(erb_flag?str_special_character1:str_special_character2);;
			String s2=!enable_special_character_flag?(erb_flag?"　":String.valueOf(c)):(erb_flag?str_special_character2:str_special_character1);;
			String temp1="";
			String temp2="";			
			for(int m=0;m<this.dc_flag;m++){
				temp1+=s1;
				temp2+=s2;
			}			
			for(int j=0;j<2;j++){				 
				 for(int k=0;k<(j==0?8:(DotMap.arraysize[num_dotarraysize]/2-8));k++){
			           if(((s[i*2+j]>>(7-k))&0x1)==1){
			           	  str_return+=temp1;
			           }else{
			        	  str_return+=temp2;
			           }
				 }
			}			
			 return str_return;
		}
		private String tomirror(String str_in){
			if(str_in==null||str_in.equals("")){
					return "";
			}
			else{
					int i=0;
					String str_out="";		
					boolean  onceagain=true;
					
					String temp1=str_in, temp2=str_in;
					while(onceagain){							
							if(temp1.contains("\n")){
									temp2=temp1.substring(0, temp1.indexOf("\n"));
									temp1=temp1.substring(temp1.indexOf("\n")+1, temp1.length());
							}
							else{
									temp2=temp1;
									onceagain=false;
							}
							for(i=temp2.length()-1;i>=0;i--)
							{
									str_out+=String.valueOf(temp2.charAt(i));								
							}
							str_out+="\n";
					}						
					return str_out;
			}
	}	

		private String toCharPic(String str_in){
			String str_out;
			try{
				str_out=cop.getStrOfPic(str_in);
			}catch(Exception e){
				e.printStackTrace();
				str_out="路径名出错！";
			}
			return str_out;
		}
}
		
	
