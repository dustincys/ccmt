﻿package p1;

import java.io.*;
import java.nio.charset.*;
import java.nio.*;

public class DotMap{
	public static int[ ] arraysize= { 24, 28, 32 };
	private static byte[ ][ ] dotMap14;
	private static byte[ ][ ] dotMap12;
	private static byte[ ][ ] dotMap16;

	static {
		try {
			FileInputStream dotMapFile12= new FileInputStream(
					"hzk12" );
			FileInputStream dotMapFile14= new FileInputStream(
					"hzk14" );
			FileInputStream dotMapFile16= new FileInputStream(
					"hzk16" );
			dotMap12= new byte[dotMapFile12.available( )][24];
			dotMap14= new byte[dotMapFile14.available( )][28];
			dotMap16= new byte[dotMapFile16.available( )][32];
			for ( int i= 0; i<dotMap12.length; i++ ) {
				dotMapFile12.read( dotMap12[i] );
			}
			for ( int i= 0; i<dotMap14.length; i++ ) {
				dotMapFile14.read( dotMap14[i] );
			}
			for ( int i= 0; i<dotMap16.length; i++ ) {
				dotMapFile16.read( dotMap16[i] );
			}
			dotMapFile12.close( );
			dotMapFile14.close( );
			dotMapFile16.close( );
		} catch( FileNotFoundException ex ) {
			ex.printStackTrace( );
		} catch( IOException ex ) {
			ex.printStackTrace( );
		}
	}


	public static byte[ ] getDotMap( char c, int flag ) throws Exception{// flag
										// 为标志为dotarraysize的标志
/*
 * 占字节数分别为24,28,32  对应12*12 14*14 16*16
 * 12行*（8bit+8bit） 14*（8bit+8bit） 16*（8bit+8bit）
 * */
		byte[ ] buffer= new byte[arraysize[flag]];
		byte[ ][ ][ ] dm_array= { dotMap12, dotMap14, dotMap16 };
		int gbCode= Charset.forName( "GB2312" )
				.encode( String.valueOf( c ) ).getShort( )&0xFFFF;
		ByteBuffer buf= ByteBuffer.wrap( dm_array[flag][((gbCode>>8)*94+(gbCode&0xFF))-0x3BBF] );
		for ( int i= 0; i<arraysize[flag]; i++ ) {
			buffer[i]= buf.get( );
		}
		return buffer;
	}
}
